templates.ErrorPageTpl = _.template([
    '<div class="error404">',
    '   <img src="/img/404.png">',
    '   <div><span>404</span></div>',
    '</div>'
].join(''));